# What is this plugin? #

This plugin shows how to write a service that can send an email. 

In theory it could be used by other classes (e.g. WebWork actions) to send an email.

You can find the full instructions for this tutorial here:

https://developer.atlassian.com/display/CONFDEV/Sending+Emails+in+a+Plugin

Also, take a look at the tests for this plugin to learn more about how to use the service.

